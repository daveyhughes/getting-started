===============================================================================
                                      git
                                by Davey Hughes
===============================================================================

                                March 24, 2016
                                 Version 1.0.0

                       Original version written for CS51

===============================================================================

-=-= TABLE OF CONTENTS =-=-

===============================================================================

Hey all! I know that many of you are probably a bit confused about how git
works, but considering that it's a core aspect of this class, as well as many
other CS courses at Harvard and as general coding workflows in the real world,
it's a really good idea to become comfortable with Git. Git is a very powerful
tool, and it provides you with a lot of tools to control your repository, but
that also means it can be confusing to figure out what you actually need to do
to use Git well.

It's important to at least feel comfortable with what Git is doing, even if you
don't know all the commands that well, so you can avoid problems and you can
fix any problems you have without breaking your code. It can be extremely
frustrating to discover a problem that involves using Git while it's 2AM the
day before a difficult pset is due.

https://xkcd.com/1597/ <--- avoid this.

First of all, I would definitely recommend reading the Getting Started and
Collaborating sections of this guide: https://www.atlassian.com/git/tutorials/
It's sort of intended to be written for someone who used Subversion (svn) in
the past, so don't worry when they make references to that. Pset0 is really
intended to be about learning the basics of installing OCaml, using the OCaml
interpreter, building OCaml code, and using Git. Even if you find that you
don't have a hard time with most of pset0, you should probably spend time this
week while the coding is easy to learn the tools, so that you aren't frustrated
with Git when the coding gets hard.

Before you read the tutorial, let's contextualize Git a little bit, and take
this time to augment some of the things that the Atlassian tutorial doesn't
really cover, or in my opinion, doesn't cover well.

Git was designed by Linus Torvalds (the same guy who started Linux), to host
the Linux kernel source code, after he became frustrated with BitKeeper. Git is
a version control system for source code, and the way you should use Git should
reflect this attitude. Git is different from other version control systems in
that each commit is a full snapshot of the project, so technically each commit
can be fully independent. This contrasts systems that only save the differences
of each file, and need the whole history to actually build a file with all its
changes.

What this means, however, is if you add a very large file to your commit, such
as a binary file or images or anything that's not plain source code, every
single commit you make after that will save a full version of that large file.
Even though Git compresses its commits, images, binary files, and the like
don't actually compress that well, so your repo will become very large very
fast. Git shouldn't be used just for backups like you might use Dropbox for.
Git makes backups of versions of your code. You don't need those compiled
binaries since at a given commit, you can just use the source code to build the
binaries again.

Another thing about Git (that's covered very well in the Atlassian Tutorial) is
that when you clone a repository, you have a full copy of that repo. The only
difference between a repo on Bitbucket or GitHub and one on your computer is
the one on these websites is a bare repository, meaning they're not designed to
actually work on directly, bare repositories get updated when cloned
repositories push to them. Now I think we have a good sense of our context.

Firstly, set up your username and email. Many people use their full name for
their username. Do this by:
> git config --global user.name "yourname" git config --global user.email
> "youremail@college.harvard.edu"
You can use a different email than your Harvard email, however since you signed
up for Bitbucket with your Harvard email, Bitbucket will do nice things by
showing your Bitbucket profile as the author on commits by cross-referencing
the email on the commit.

So you've cloned the repository that CS51 gave you. How do you proceed? Well,
in the simplest workflow, you can just make some changes, add files to the
"staging area", commit whatever files are "staged", and then push the commits
to the repo. But that's actually kind of complicated, and it's easy to just
want everything to "work", and if you aren't aware of what's happening, you
might find yourself accidentally destroying code that you spent hours writing.

============================

Let's get a picture of the three main parts of a Git repo on your computer: the
commit history, the staging area, and your working directory. I don't feel that
many tutorials really explain these parts, so this is a good time to pay close
attention. For now, let's pretend the staging area doesn't exist, and let's
make an analogy between the commits and your working directory with you editing
a simple word document. Nowadays programs often save automatically, but let's
pretend they don't. You have to save manually.

The working directory is like your open file. If you make changes, but don't
close Word, then your changes are still there. Once you save, however, you
could go back to that saved file and see what exactly you saved. In this
analogy, committing is like saving, but every time you save you give the file a
new name. This would allow you to see every time you saved the file. But let's
say you wrote some new things, didn't save them, and then wanted to see what
you did 2 saves ago. Word allows you to have multiple open documents, but in
this analogy, you can only have one document open at once. If you don't save
your current work, looking at an old version would mean throwing away your
current work! This is why Git says you can't look at old commits without
committing your current work. Your working directory is really just a copy of
whatever commit you're looking at.

At any time, use:
> git status
To view the staging area messages, and
> git log
To view your past commits.

So Git is more complicated than this simple analogy. That's why the "staging
area" exists. You have a lot of files in your directory, but maybe you only
wanted to commit the files that you actually made progress on, and you didn't
want to commit some other files that were just some testing things or only half
complete. You simply only run "git add" for the files that you want to commit.
Let's diagram this: Working directory: file1, file2, file3, file4, file5

History: Commit3 file1, file2, file3

Commit2 file1, file2, file3

Commit1 file1, file2, file3 So you're ready to add commit4, but even though you
made changes to all these files, you're not happy with what you changed in
file1. You would like commit4 to contain your changes to file2, file3, and the
new file4, but use the commit3's file1, and just not use file5. This sounds
complicated, but it's actually very easy when you think about the staging area
as being a way to describe what changes from your working directory that you
want to put in the commit. Any file that was in a previous commit, but you
didn't add for staging will just use the file from the last commit. Let's see
this in action:
> git add file2 file3 file4
So now here's what the structure looks like: Working directory: file1, file2,
file3, file4, file5

Staging area: file2, file3, file4

History: Commit3 file1, file2, file3

Commit2 file1, file2, file3

Commit1 file1, file2, file3 And then when you run:
> git commit
After you add your commit message, things will look like this: Working
directory: file1, file2, file3, file4, file5

History: Commit4 file1, file2, file3, file4 <--- this file1 is a copy of the
one from commit3, not the working directory

Commit3 file1, file2, file3

Commit2 file1, file2, file3

Commit1 file1, file2, file3 So don't just blindly run "git add --all" and then
"git commit -a", since making a commit is a great time to choose which files
you really want to put into a commit. This makes it easier to be able to look
at an old commit and see changes rather than making messy, confusing commits.
If you've added a file to the staging area that you don't actually want to
commit, you can do
> git reset <file>
And that'll just take it out of the staging area. Note however that the staging
area is, in itself, sort of a "snapshot" of files. If you add a file to the
staging area, but then make changes in your working directory, those changes
aren't reflected in the file that's in the staging area. You'd have to "git
add" that file again to update the staging area. You can also just run:
> git reset
To take all files out of the staging area.

The last thing I'd like to teach you is how to write good commit messages. Even
though CS51 won't care about your commit messages, and likely during the class
nobody will, it's good practice for yourself. It's like writing good code, and
writing good comments. Even if nobody else cares or is grading you on it, you
should do it anyway since it'll help you when you have to read your old code,
or in this case, old commits.

I would recommend this guide, but I'll also highlight the main points.
http://chris.beams.io/posts/git-commit Separate subject from body with a blank
line Limit the subject line to 50 characters Capitalize the subject line Do not
end the subject line with a period Use the imperative mood in the subject line
Wrap the body at 72 characters Use the body to explain what and why vs. how

I'm not sure about other text editors, but Vim will even syntax highlight to
show you if you're over 50 characters on the first line, and over 72 characters
on the other lines. This is how standard the formatting system is.

The last thing is using "git push" and "git pull". Pushing is really just
telling a remote repository (such as the one on Bitbucket) to add your local
commits to its commit history. Pulling is getting the latest changes from the
remote repository. If you're the only person using the remote repository,
things should be simple, since there won't be any unexpected changes to the
remote repository. But, if you're working on the same remote repository with
someone else, that person may push some changes to the remote that you don't
have. So if you know you're working in that environment, it's a good idea to
run "git pull" before pushing your own work. You will have to merge your
changes manually, which means going through each change with a text editor and
manually choosing which bits you would like to keep, and which to delete.
Merging is another topic, however, and a bit beyond the scope of this guide.

So that's it for now! This is only the bare basics of Git, and there are other
powerful features that you should learn about, but this is probably okay as far
as CS51 goes. I have by no means mastered Git, but I've definitely benefited
from learning more about it, rather than only knowing the basic tools that are
required to submit homework. Thinking of Git as a source code version control
system will definitely help you understand why Git operates the way that it
does, and hopefully prevent you from having issues when you use it to submit
homework.

Feel free to ask me questions as well. Each staff member has their own level of
knowledge of Git, so I can't make any guarantees about that, but I'm sure the
staff would also be willing to help with better understanding as well.
